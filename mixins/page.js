import { mapGetters } from 'vuex'

export default {
  data () {
    return {}
  },
  components: {
    Header: () => import('~/components/header')
  },
  computed: {
    price () {},
    ...mapGetters({})
  }
}
