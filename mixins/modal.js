
export default {
  props: {
    required: {
      type: Boolean,
      default: false
    },
    value: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      dialog: false,
      dialogLock: false
    }
  },
  watch: {
    value (val) {
      this.dialog = val
      this.dialogLock = true
      this.$nextTick(() => this.$nextTick(() => {
        this.dialogLock = false
      }))
    },
    dialog (val) {
      if (this.dialogLock) {
        return
      }
      this.$emit('input', val)
    }
  }
}
