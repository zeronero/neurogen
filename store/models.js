import Vue from 'vue'

export const state = () => ({
  list: []
})

export const getters = {
  list: state => state.list,
  count: (state, getters) => getters.list.length,
  isEmpty: (state, getters) => !getters.count
}

export const mutations = {
  /*
    Иначе не всегда сробатывает реактивность
  */
  ADD_NOTIFICATION (state, model) {
    Vue.set(state.list, state.list.length, model)
  },
  SET_NOTIFICATIONS (state, arr) {
    state.list = arr
  }
}

export const actions = {
  addNotification ({ commit }, model) {
    commit('ADD_NOTIFICATION', model)
  },
  setNotifications ({ commit }, arr) {
    commit('SET_NOTIFICATIONS', arr)
  }
}
