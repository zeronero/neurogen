import Vue from 'vue'

export const state = () => ({
  list: [12]
})

export const getters = {
  list: state => state.list,
  count: (state, getters) => getters.list.length,
  isEmpty: (state, getters) => !getters.count
}

export const mutations = {
  /*
    Иначе не всегда сробатывает реактивность
  */
  ADD_NOTIFICATION (state, notification) {
    Vue.set(state.list, state.list.length, notification)
  },
  SET_NOTIFICATIONS (state, arr) {
    state.list = arr
  }
}

export const actions = {
  addNotification ({ commit }, notification) {
    commit('ADD_NOTIFICATION', notification)
  },
  setNotifications ({ commit }, arr) {
    commit('SET_NOTIFICATIONS', arr)
  }
}
