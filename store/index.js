export const state = () => ({
  sidebar: true,
  notificationShow: false
})

export const getters = {
  sidebar: state => state.sidebar,
  notificationShow: state => state.notificationShow
}

export const mutations = {
  TOGGLE_SIDEBAR (state) {
    state.sidebar = !state.sidebar
  },
  SET_SIDEBAR (state, val) {
    state.sidebar = val
  },
  SET_NOTIFICATION_SHOW (state, val) {
    state.notificationShow = val
  }
}

export const actions = {
  toggleSidebar ({ commit }) {
    commit('TOGGLE_SIDEBAR')
  },
  setSidebar ({ commit }) {
    commit('SET_SIDEBAR')
  },
  setNotificationShow ({ commit }, val) {
    commit('SET_NOTIFICATION_SHOW', val)
  }
}
