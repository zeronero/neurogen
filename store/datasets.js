import { nanoid } from 'nanoid'
import Vue from 'vue'

export const state = () => ({
  list: [],
  progress: 0,
  loading: false
})

export const getters = {
  list: state => state.list,
  count: (state, getters) => getters.list.length,
  isEmpty: (state, getters) => !getters.count,
  progress: state => state.progress,
  loading: state => state.loading
}

export const mutations = {
  /*
    Иначе не всегда сробатывает реактивность
  */
  ADD_DATASET (state, dataset) {
    /* TODO УБРАТЬ КАК СДЕЛАЮ ЗАГРУЗКУ НА СЕРВЕР */
    dataset = {
      _id: nanoid(),
      ...dataset
    }
    Vue.set(state.list, state.list.length, dataset)
  },
  PUT_DATASET (state, dataset) {
    const index = state.list.findIndex(x => x._id === dataset._id)
    Vue.set(state.list, index, dataset)
  },
  SET_PROGRESS (state, proc) {
    state.progress = proc
  },
  SET_LOADING (state, loading) {
    state.loading = loading
  },
  SET_DATASETS (state, arr) {
    state.list = arr
  },
  REMOVE_DATASET (state, dataset) {
    Vue.set(state, 'list', state.list.filter(val => val._id !== dataset._id))
  }
}

export const actions = {
  async addDataset ({ commit }, dataset) {
    const form = new FormData()
    form.append('name', dataset.name)
    form.append('type', dataset.type)
    form.append('description', dataset.description)

    if (dataset.dataset !== null && dataset.dataset.constructor === File) {
      form.append('dataset', dataset.dataset)
    }

    if (dataset.transformer !== null && dataset.transformer.constructor === File) {
      form.append('transformer', dataset.transformer)
    }

    commit('SET_LOADING', true)
    const { data } = await this.$axios.post('api/datasets', form, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress (progressEvent) {
        console.info(progressEvent)
        commit('SET_PROGRESS', Math.round((progressEvent.loaded * 100) / progressEvent.total))
      }
    })

    commit('SET_LOADING', false)
    commit('SET_PROGRESS', 0)

    if (dataset.dataset !== null && dataset.dataset.constructor === File) {
      dataset.dataset = {
        name: dataset.dataset.name
      }
    }

    if (dataset.transformer !== null && dataset.transformer.constructor === File) {
      dataset.transformer = {
        name: dataset.transformer.name
      }
    }

    if (!data.error) {
      commit('ADD_DATASET', dataset)
    }
  },
  async getDataset ({ commit }) {
    const { data } = await this.$axios.get('api/datasets')
    if (!data.error) {
      commit('SET_DATASETS', data.data)
    }
  },
  async putDataset ({ commit }, dataset) {
    const form = new FormData()
    form.append('name', dataset.name)
    form.append('type', dataset.type)
    form.append('description', dataset.description)

    if (dataset.dataset !== null && dataset.dataset.constructor === File) {
      form.append('dataset', dataset.dataset)
    }

    if (dataset.transformer !== null && dataset.transformer.constructor === File) {
      form.append('transformer', dataset.transformer)
    }

    commit('SET_LOADING', true)
    const { data } = await this.$axios.put(`api/datasets/${dataset._id}`, form, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress (progressEvent) {
        console.info(progressEvent)
        commit('SET_PROGRESS', Math.round((progressEvent.loaded * 100) / progressEvent.total))
      }
    })

    commit('SET_LOADING', false)
    commit('SET_PROGRESS', 0)

    console.info(data)

    if (dataset.dataset !== null && dataset.dataset.constructor === File) {
      dataset.dataset = {
        name: dataset.dataset.name
      }
    }

    if (dataset.transformer !== null && dataset.transformer.constructor === File) {
      dataset.transformer = {
        name: dataset.transformer.name
      }
    }

    if (!data.error) {
      commit('PUT_DATASET', dataset)
    }
  },
  setDatasets ({ commit }, datasets) {
    commit('SET_DATASETS', datasets)
  },
  async removeDataset ({ commit }, datasets) {
    await this.$axios.delete(`api/datasets/${datasets._id}`)
    commit('REMOVE_DATASET', datasets)
  }
}
